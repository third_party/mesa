
#ifndef _DRM_IMAGE_FORMAT_MODIFIER_H_
#define _DRM_IMAGE_FORMAT_MODIFIER_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Carries information from VkImageDrmFormatModifierExplicitCreateInfoEXT.
 */
struct drm_image_format_modifier
{
   uint64_t modifier;
   uint64_t offset;
   uint64_t size;
   uint64_t row_pitch;
};

#ifdef __cplusplus
}
#endif

#endif /* _DRM_IMAGE_FORMAT_MODIFIER_H_ */
