/*
 * Copyright © 2023 Google, LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#pragma once

#include <zircon/types.h>

#include <lib/magma/magma_sysmem.h>

#include "vulkan/vulkan_core.h"
#include "vulkan/vulkan_fuchsia.h"

#include "vk_object.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Provided by VK_FUCHSIA_buffer_collection. */
VkResult lvp_CreateBufferCollectionFUCHSIA(
    VkDevice                                    device,
    const VkBufferCollectionCreateInfoFUCHSIA*  pCreateInfo,
    const VkAllocationCallbacks*                pAllocator,
    VkBufferCollectionFUCHSIA*                  pCollection);

/* Provided by VK_FUCHSIA_buffer_collection. */
void lvp_DestroyBufferCollectionFUCHSIA(
    VkDevice                                    device,
    VkBufferCollectionFUCHSIA                   collection,
    const VkAllocationCallbacks*                pAllocator);

/* Provided by VK_FUCHSIA_buffer_collection. */
VkResult lvp_GetBufferCollectionPropertiesFUCHSIA(
    VkDevice                                    device,
    VkBufferCollectionFUCHSIA                   collection,
    VkBufferCollectionPropertiesFUCHSIA*        pProperties);

/* Provided by VK_FUCHSIA_buffer_collection. */
VkResult lvp_SetBufferCollectionBufferConstraintsFUCHSIA(
    VkDevice                                    device,
    VkBufferCollectionFUCHSIA                   collection,
    const VkBufferConstraintsInfoFUCHSIA*       pBufferConstraintsInfo);

/* Provided by VK_FUCHSIA_buffer_collection. */
VkResult lvp_SetBufferCollectionImageConstraintsFUCHSIA(
    VkDevice                                    device,
    VkBufferCollectionFUCHSIA                   collection,
    const VkImageConstraintsInfoFUCHSIA*        pImageConstraintsInfo);

#define MAX_BUFFER_COLLECTION_FORMAT_INDICES 128

struct lvp_buffer_collection {
   struct vk_object_base base;

   magma_buffer_collection_t buffer_collection;
   magma_sysmem_buffer_constraints_t constraints;
   uint32_t format_index_input_index_map[MAX_BUFFER_COLLECTION_FORMAT_INDICES];
};

VK_DEFINE_NONDISP_HANDLE_CASTS(lvp_buffer_collection, base, VkBufferCollectionFUCHSIA,
                               VK_OBJECT_TYPE_BUFFER_COLLECTION_FUCHSIA)

struct lvp_device;
struct lvp_device_memory;

/* Called by lvp_AllocateMemory().  Obtains a VMO from the buffer_collection and passes it to
   `lvp_fuchsia_import_mem()`.  Constraints must have been set on the buffer collection before
   calling this, otherwise it will hang in `WaitForBuffersAllocated()`.

   NOTE: the `type_index` field is left untouched; the caller is responsible for setting it.
*/
VkResult
fuchsia_allocate_memory_from_buffer_collection(
    const struct lvp_device* device,
    const struct lvp_buffer_collection* buffer_collection,
    uint32_t index,
    uint64_t alloc_size,
    struct lvp_device_memory* memory_out);

#ifdef __cplusplus
}
#endif
