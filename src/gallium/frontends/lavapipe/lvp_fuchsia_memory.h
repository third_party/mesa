/*
 * Copyright © 2023 Google, LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#pragma once

#include <zircon/types.h>

#include "vulkan/vulkan_core.h"
#include "vulkan/vulkan_fuchsia.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
   Attempt to map the VMO to a memory of length (at least) `len`.  If successful, returns VK_SUCCESS
   and stores a pointer to the mapped memory in `ptr_out`.  On error, closes the VMO handle (unless
   the handle is bad), and returns one of:
   - VK_ERROR_INITIALIZATION_FAILED if the handle is bad
   - VK_ERROR_OUT_OF_HOST_MEMORY for any memory-related error, including if the VMO is too small
   - VK_UNKNOWN for all other errors
*/
VkResult lvp_fuchsia_map_vmo(zx_handle_t vmo, size_t len, zx_vaddr_t* ptr_out);

/*
    Return the size of the VMO.  Prefers to return the "content size", if available.  Otherwise,
    return the result of `zx_vmo_get_size()`.

    - if anything other than VK_SUCCESS is returned, the `vmo` handle will be closed.
*/
VkResult lvp_fuchsia_get_vmo_size(zx_handle_t vmo, uint64_t* size_out);


struct lvp_device_memory;

/* Called by (or on behalf of) lvp_AllocateMemory().  Maps the provide VMO, and populates the
   provided `memory_out` struct.

   NOTE: the `type_index` field is left untouched; the caller is responsible for setting it.
*/
VkResult lvp_fuchsia_import_vmo(
    zx_handle_t vmo,
    uint64_t allocation_size,
    struct lvp_device_memory* memory_out);

#ifdef __cplusplus
}
#endif
