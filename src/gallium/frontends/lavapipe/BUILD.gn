# Copyright 2023 Google, LLC
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.

import("../../../../mesa.gni")

mesa_python_action("lvp_entrypoints") {
  outputs = [
    "$target_gen_dir/lvp_entrypoints.h",
    "$target_gen_dir/lvp_entrypoints.c",
  ]

  script = "$mesa_build_root/src/vulkan/util/vk_entrypoints_gen.py"

  inputs = [
    "$mesa_build_root/src/vulkan/util/vk_dispatch_table_gen.py",
    "$mesa_build_root/src/vulkan/util/vk_extensions.py",
    "$mesa_build_root/src/vulkan/util/vk_entrypoints.py",
    "$mesa_build_root/src/vulkan/registry/vk.xml",
  ]

  args = [
    "--xml",
    rebase_path("$mesa_build_root", root_build_dir) +
        "/src/vulkan/registry/vk.xml",
    "--proto",
    "--weak",
    "--out-h",
    rebase_path("$target_gen_dir/lvp_entrypoints.h", root_build_dir),
    "--out-c",
    rebase_path("$target_gen_dir/lvp_entrypoints.c", root_build_dir),
    "--prefix",
    "lvp",
  ]
}

config("lavapipe_config") {
  include_dirs = [
    "$root_gen_dir/third_party/mesa/src/compiler/nir/",
    "$root_gen_dir/third_party/mesa/src/gallium/frontends/lavapipe/",
    "$root_gen_dir/third_party/mesa/src/vulkan/runtime/",
    "$root_gen_dir/third_party/mesa/src/vulkan/util/",
    "$mesa_build_root/include/",
    "$mesa_build_root/src/compiler/",
    "$mesa_build_root/src/compiler/nir/",
    "$mesa_build_root/src/gallium/auxiliary/",
    "$mesa_build_root/src/gallium/drivers/",
    "$mesa_build_root/src/gallium/include/",
    "$mesa_build_root/src/gallium/winsys/",
    "$mesa_build_root/src/util/",
    "$mesa_build_root/src/",
    "$mesa_build_root/src/vulkan/util/",
  ]

  defines = [
    # Unnecessary because we hardcode GALLIUM_STATIC_TARGETS=1.
    "PIPE_SEARCH_DIR=\"UNNECESSARY\"",

    # TODO(https://fxbug.dev/321122796): otherwise get "use of undeclared identifier 'NULL'"
    # in u_dl.c etc.  Is there some better way to define this?  If we do decide to define NULL
    # (vs e.g. adding a header include), is this the best place?  Probably, since this is closest
    # to where it's needed.
    "NULL=0",

    # So that sw_screen_create_vk() is customized to use lavapipe.
    "GALLIUM_LLVMPIPE",
  ]

  # Suppress warnings.
  configs = [
    "//build/config:Wno-unused-but-set-variable",
    "//build/config:Wno-strict-prototypes",
    "//third_party/mesa:LLVMHeader_config",
  ]
}

mesa_source_set("lavapipe") {
  public_deps = [ "$mesa_build_root/src/compiler" ]

  public_configs = [ ":lavapipe_config" ]

  deps = [
    ":fuchsia_buffer_collection_extension",
    ":lvp_entrypoints",
    "$mesa_build_root/src:sha1",
    "$mesa_build_root/src/compiler/nir",
    "$mesa_build_root/src/gallium/auxiliary",
    "$mesa_build_root/src/gallium/auxiliary/pipe-loader:pipe-loader-llvmpipe",
    "$mesa_build_root/src/util:zircon_util",
    "$mesa_build_root/src/vulkan/util",
  ]

  sources = [
    # target.c is not used by our sibling meson.build, but rather by
    # //mesa/src/gallium/targets/lavapipe/meson.build.  We include it here because it's not worth
    # creating another BUILD.gn for a single file.
    "$mesa_build_root/src/gallium/targets/lavapipe/target.c",
    "$target_gen_dir/lvp_entrypoints.c",
    "$target_gen_dir/lvp_entrypoints.h",
    "lvp_cmd_buffer.c",
    "lvp_descriptor_set.c",
    "lvp_device.c",
    "lvp_execute.c",
    "lvp_formats.c",
    "lvp_fuchsia.c",
    "lvp_fuchsia_memory.c",
    "lvp_image.c",
    "lvp_inline_uniforms.c",
    "lvp_lower_input_attachments.c",
    "lvp_lower_vulkan_resource.c",
    "lvp_lower_vulkan_resource.h",
    "lvp_pipe_sync.c",
    "lvp_pipeline.c",
    "lvp_pipeline_cache.c",
    "lvp_query.c",
    "lvp_util.c",
    "lvp_wsi.c",
  ]

  if (target_os == "fuchsia") {
    public_deps += [ "$mesa_build_root/src/vulkan/wsi:stub" ]
    deps += [ "$mesa_build_root/src/vulkan/runtime:zircon" ]
    sources += [ "lvp_pipe_sync_fuchsia.c" ]
  } else {
    public_deps += [ "$mesa_build_root/src/vulkan/wsi" ]
    deps += [ "$mesa_build_root/src/vulkan/runtime" ]
  }
}

# Implements VK_FUCHSIA_buffer_collection.
#
# Unlike most ICDs, the Lavapipe ICD runs entirely on the CPU in the client process, without the
# need to communicate with the privileged system driver.  Therefore it would seem that Magma, whose
# job is to provide a framework to communicate with the system driver, is unnecessary in Fuchsia.
# This is true.  However, to support VK_FUCHSIA_buffer_collection, Lavapipe must interact with
# sysmem.  Magma provides sysmem utilities which make this job much easier.
mesa_source_set("fuchsia_buffer_collection_extension") {
  sources = [ "lvp_fuchsia_buffer_collection.c" ]

  deps = [
    ":lvp_entrypoints",

    # See target-level comment for rationale of use of Magma in Lavapipe.
    "$mesa_build_root/include:vulkan",
    "//src/graphics/lib/magma/src/libmagma:magma_sysmem",
    "//src/graphics/magma/lib/magma/platform/zircon:logger_for_stderr",
  ]

  include_dirs = [
     # For LP_RASTER_BLOCK_SIZE, defined in "lp_rast.h"
    "$mesa_build_root/src/gallium/drivers/llvmpipe/",

    "$root_gen_dir/third_party/mesa/src/gallium/frontends/lavapipe/"
  ]

  if (target_os == "fuchsia") {
    deps += [
      "$mesa_build_root/src/vulkan/runtime:zircon",
      "$mesa_build_root/src/vulkan/wsi:stub",
    ]
  } else {
    deps += [
      "$mesa_build_root/src/vulkan/runtime",
      "$mesa_build_root/src/vulkan/wsi",
    ]
  }

  public_deps = [
    # See target-level comment for rationale of use of Magma in Lavapipe.
    "//sdk/lib/magma_client:magma_headers",
  ]

  configs = [
    ":lavapipe_config",
    "//third_party/mesa:LLVMHeader_config",
  ]
}
