/*
 * Copyright © 2023 Google, LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#pragma once

#include <llvm/ExecutionEngine/SectionMemoryManager.h>

#include <lib/zx/resource.h>

#include <cstddef>
#include <system_error>

class LavapipeMemoryMapper : public llvm::SectionMemoryManager::MemoryMapper {
public:
   LavapipeMemoryMapper();

   llvm::sys::MemoryBlock
   allocateMappedMemory(llvm::SectionMemoryManager::AllocationPurpose Purpose, size_t NumBytes,
                        const llvm::sys::MemoryBlock* const NearBlock, unsigned Flags,
                        std::error_code& EC) override;

   std::error_code protectMappedMemory(const llvm::sys::MemoryBlock& Block,
                                       unsigned Flags) override;

   std::error_code releaseMappedMemory(llvm::sys::MemoryBlock& M) override;

private:
   zx::resource vmex_;
   // TODO(https://fxbug.dev/321122796): temporary debugging to find potential memory leaks.
   // Can be removed.
   size_t allocation_count_ = 0;
   size_t release_count_ = 0;
};
