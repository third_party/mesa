/*
 * Copyright © 2018 Intel Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "intel_perf.h"

uint32_t intel_perf_get_n_passes(struct intel_perf_config *perf,
                               const uint32_t *counter_indices,
                               uint32_t counter_indices_count,
                               struct intel_perf_query_info **pass_queries)
{
  return 0;
}

void intel_perf_get_counters_passes(struct intel_perf_config *perf,
                                  const uint32_t *counter_indices,
                                  uint32_t counter_indices_count,
                                  struct intel_perf_counter_pass *counter_pass)
{}

void intel_perf_query_result_accumulate(struct intel_perf_query_result *result,
                                        const struct intel_perf_query_info *query,
                                        const uint32_t *start,
                                        const uint32_t *end)
{}

void intel_perf_query_result_clear(struct intel_perf_query_result *result) {}

void intel_perf_init_metrics(struct intel_perf_config *perf_cfg,
                           const struct intel_device_info *devinfo,
                           int drm_fd,
                           bool include_pipeline_statistics,
                           bool use_register_snapshots)
{}

void intel_perf_query_result_accumulate_fields(struct intel_perf_query_result *result,
                                               const struct intel_perf_query_info *query,
                                               const void *start,
                                               const void *end,
                                               bool no_oa_accumulate)
{}
