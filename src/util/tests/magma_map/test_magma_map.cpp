/*
 * Copyright © 2022 Google, LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "util/magma/u_magma_map.h"
#include "gtest/gtest.h"

class TestMagmaMap : public testing::Test {
public:
   void SetUp() override {
      u_magma_map_init(&map_);
   }

   void TearDown() override {
      u_magma_map_release(&map_);
   }

   struct u_magma_map map_;
};

TEST_F(TestMagmaMap, QueryInvalid) {
   EXPECT_EQ(0u, u_magma_map_query(&map_, 0, nullptr));
}

TEST_F(TestMagmaMap, Basic) {
   constexpr int kHandle = 1;
   constexpr uint64_t kObject = 0xabcd12345678beef;
   uint64_t object;

   // Not found.
   EXPECT_FALSE(u_magma_map_query(&map_, kHandle, &object));

   EXPECT_EQ(kHandle, u_magma_map_get(&map_, kObject));

   EXPECT_TRUE(u_magma_map_query(&map_, kHandle, &object));
   EXPECT_EQ(object, kObject);

   u_magma_map_put(&map_, kHandle);

   // Not found.
   EXPECT_FALSE(u_magma_map_query(&map_, kHandle, &object));
}

TEST_F(TestMagmaMap, HandleRecycle) {
   EXPECT_EQ(1u, u_magma_map_get(&map_, 0));
   EXPECT_EQ(2u, u_magma_map_get(&map_, 0));
   u_magma_map_put(&map_, 1u);
   EXPECT_EQ(1u, u_magma_map_get(&map_, 0));
}
