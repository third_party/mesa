/*
 * Copyright © 2022 Google, LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef U_MAGMA_MAP_H
#define U_MAGMA_MAP_H

#include "util/sparse_array.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Maps 32bit handles to 64bit objects.
 * Thread-safe because it uses util_sparse_array.
 */

struct u_magma_map {
   struct util_sparse_array array;
   struct util_sparse_array_free_list free_list;
   uint32_t current_handle;
};

void u_magma_map_init(struct u_magma_map* map);
void u_magma_map_release(struct u_magma_map* map);
void u_magma_map_put(struct u_magma_map* map, uint32_t handle);

/* Returns the handle. */
uint32_t u_magma_map_get(struct u_magma_map* map, uint64_t object);

/* If found returns true and sets |object_out|. */
bool u_magma_map_query(struct u_magma_map* map, uint32_t handle, uint64_t* object_out);

#ifdef __cplusplus
}
#endif

#endif /* U_MAGMA_MAP_H */
