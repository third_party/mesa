/*
 * Copyright © 2022 Google, LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "u_magma_mmap.h"

#include <sys/mman.h> // for MAP_FAILED

#if defined(__linux__)
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#endif

#if defined(__Fuchsia__)
#include "os/fuchsia.h"
#include <zircon/syscalls.h>
#include <zircon/process.h>
#endif

#include "util/log.h"

void u_magma_close_handle(magma_handle_t handle) {
#if defined(__Fuchsia__)
   zx_handle_t vmo_handle = handle;
   zx_handle_close(vmo_handle);

#elif defined(__linux__)
   int fd = handle;
   close(fd);
#else
   #error Unsupported
#endif
}

void* u_magma_mmap_handle(magma_handle_t handle, uint64_t offset, uint64_t size) {
#if defined(__Fuchsia__)
   zx_handle_t vmo_handle = handle;

   zx_vaddr_t zx_vaddr;
   zx_status_t zx_status = zx_vmar_map(zx_vmar_root_self(), ZX_VM_PERM_READ | ZX_VM_PERM_WRITE,
                                       0 /*vmar_offset*/, vmo_handle, offset, size, &zx_vaddr);

   if (zx_status != ZX_OK) {
      mesa_logd("zx_vmar_map failed: status %d", zx_status);
      return MAP_FAILED;
   }

   void* addr = (void*) zx_vaddr;

#elif defined(__linux__)
   int fd = handle;

   void* addr = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, offset);

#else
#error Unsupported
#endif

   return addr;
}

void u_magma_munmap(void* addr, uint64_t size) {
#ifdef __Fuchsia__
   zx_status_t status = zx_vmar_unmap(zx_vmar_root_self(), (zx_vaddr_t) addr, size);
   if (status != ZX_OK) {
      mesa_logd("zx_vmar_unmap failed: %d", status);
      return;
   }
#endif

#ifdef __linux__
   int status = munmap(addr, size);
   if (status) {
      mesa_logd("munmap failed: %d", status);
      return;
   }
#endif
}
