# Copyright 2017 Google, LLC
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.

import("../../../mesa.gni")

config("config_util") {
  include_dirs = [
    "$target_gen_dir",
    ".",
  ]
}

mesa_source_set("util") {
  public_configs = [ ":config_util" ]
  public_deps = [
    "$mesa_build_root/include:vulkan",
    "$mesa_build_root/include:c_compat"
  ]
  deps = [
    "$mesa_build_root/src/util",
    "$mesa_build_root/src/compiler/nir",
  ]
  sources = [
    "$target_gen_dir/vk_enum_to_str.c",
    "$target_gen_dir/vk_dispatch_table.c",
    "$target_gen_dir/vk_dispatch_table.h",
    "$target_gen_dir/vk_extensions.h",
    "$target_gen_dir/vk_extensions.c",
    "vk_alloc.h",
    "vk_alloc.c",
    "vk_enum_to_str.h",
    "vk_format.c",
    "vk_format.h",
    "vk_util.c",
    "vk_util.h",
  ]
  deps += [
    ":gen_enum_to_str",
    ":vk_dispatch_table",
    ":vk_extensions",
  ]
  configs = [
    "//build/config:Wno-conversion",
    "//build/config:Wno-strict-prototypes",
  ]
}

mesa_python_action("gen_enum_to_str") {
  outputs = [
    "$target_gen_dir/vk_enum_to_str.h",
    "$target_gen_dir/vk_enum_to_str.c",
    "$target_gen_dir/vk_enum_defines.h",
  ]

  script = "gen_enum_to_str.py"

  inputs = [ "$mesa_build_root/src/vulkan/registry/vk.xml" ]

  args = [
    "--xml",
    rebase_path("$mesa_build_root", root_build_dir) + "/src/vulkan/registry/vk.xml",
    "--outdir",
    rebase_path("$target_gen_dir", root_build_dir),
  ]
}

mesa_python_action("vk_dispatch_table") {
  outputs = [
    "$target_gen_dir/vk_dispatch_table.h",
    "$target_gen_dir/vk_dispatch_table.c",
  ]

  script = "vk_dispatch_table_gen.py"

  inputs = [
    "vk_extensions.py",
    "$mesa_build_root/src/vulkan/registry/vk.xml",
    "$mesa_build_root/src/vulkan/util/vk_entrypoints.py",
  ]

  args = [
    "--xml",
    rebase_path("$mesa_build_root/src/vulkan/registry/vk.xml", root_build_dir),
    "--out-c",
    rebase_path("$target_gen_dir/vk_dispatch_table.c", root_build_dir),
    "--out-h",
    rebase_path("$target_gen_dir/vk_dispatch_table.h", root_build_dir),
  ]
}

mesa_python_action("vk_extensions") {
  outputs = [
    "$target_gen_dir/vk_extensions.h",
    "$target_gen_dir/vk_extensions.c",
  ]

  script = "vk_extensions_gen.py"

  inputs = [ "vk_extensions.py", "$mesa_build_root/src/vulkan/registry/vk.xml" ]

  args = [
    "--xml",
    rebase_path("$mesa_build_root/src/vulkan/registry/vk.xml", root_build_dir),
    "--out-c",
    rebase_path("$target_gen_dir/vk_extensions.c", root_build_dir),
    "--out-h",
    rebase_path("$target_gen_dir/vk_extensions.h", root_build_dir),
  ]
}
