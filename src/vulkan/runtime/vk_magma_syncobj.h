/*
 * Copyright © 2022 The Fuchsia Authors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#ifndef VK_MAGMA_SYNCOBJ_H
#define VK_MAGMA_SYNCOBJ_H

#include "vk_sync.h"

#include <lib/magma/magma.h>

#include "util/macros.h"

#ifdef __cplusplus
extern "C" {
#endif

struct vk_magma_syncobj {
   struct vk_sync base;
   magma_semaphore_t semaphore;
   magma_semaphore_id_t id;
};

void vk_magma_syncobj_finish(struct vk_device *device,
                             struct vk_sync *sync);

static inline bool
vk_sync_type_is_magma_syncobj(const struct vk_sync_type *type)
{
   return type->finish == vk_magma_syncobj_finish;
}

static inline struct vk_magma_syncobj *
vk_sync_as_magma_syncobj(struct vk_sync *sync)
{
   if (!vk_sync_type_is_magma_syncobj(sync->type))
      return NULL;

   return container_of(sync, struct vk_magma_syncobj, base);
}

struct vk_sync_type vk_magma_syncobj_get_type(void);

#ifdef __cplusplus
}
#endif

#endif /* VK_MAGMA_SYNCOBJ_H */
